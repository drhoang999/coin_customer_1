import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/config_controller.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';

class HomeStyle1Controller extends GetxController {
  DataAppCustomerController dataAppCustomerController = Get.find();
  CustomerConfigController configController = Get.find();
  var opacity = .0.obs;
  var isRefresh = false.obs;

  void changeOpacitySearch(double va) {
    opacity.value = va;
  }

  Future<void> refresh() async {
    await configController.getAppTheme(refresh: true);
    await dataAppCustomerController.getHomeData();
    await dataAppCustomerController.getBadge();
    await dataAppCustomerController.getInfoCustomer();
    isRefresh.value = !isRefresh.value;
  }
}
