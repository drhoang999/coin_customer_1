import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:sahashop_customer/app_customer/components/product_item/post_item_widget.dart';
import 'package:sahashop_customer/app_customer/model/button_home.dart';
import 'package:sahashop_customer/app_customer/model/category.dart';
import 'package:sahashop_customer/app_customer/model/post.dart';
import 'package:sahashop_customer/app_customer/model/product.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/home_buttons/list_home_button.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/product_item_widget/product_item_widget.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';
import 'package:sahashop_customer/app_customer/utils/action_tap.dart';

class HomeBodyWidget extends StatelessWidget {
  DataAppCustomerController dataAppCustomerController = Get.find();

  List<Widget> listLayout() {
    List<Widget> list = [];
    if (dataAppCustomerController.homeData!.listLayout != null) {
      for (var layout in dataAppCustomerController.homeData!.listLayout!) {
        if (layout.hide == true || layout.list!.length == 0) {
          list.add(Container());
          continue;
        }

        list.add(layout.model == "HomeButton"
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15.0, left: 8),
                      child: Text(
                        "Tiện ích của tôi",
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    ListHomeButtonWidget(),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 8,
                      color: Colors.grey[100],
                    ),
                  ],
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10, right: 15, top: 10, bottom: 10),
                    child: SectionTitle(
                      title: layout.title ?? "",
                      titleEnd: "Tất cả",
                      colorTextMore: Colors.blueAccent,
                      pressTitleEnd: () {
                        ActionTap.onTap(
                          typeAction:
                              layout.typeActionMore ?? "CATEGORY_PRODUCT",
                          value: "",
                        );
                      },
                    ),
                  ),
                  if (layout.model == "Product")
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 5, top: 8, right: 0, bottom: 8),
                          child: Container(
                            height: dataAppCustomerController
                                        .infoCustomer.value.isCollaborator ==
                                    true
                                ? 315
                                : 251,
                            width: layout.list!.cast<Product>().length * 180,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: layout.list!
                                    .cast<Product>()
                                    .map((product) => ProductItemWidget(
                                          width: 180,
                                          product: product,
                                        ))
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 8,
                          color: Colors.grey[100],
                        ),
                      ],
                    ),
                  if (layout.model == "Category")
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 8.0, left: 8.0, right: 8.0),
                          child: Container(
                            width: Get.width,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: layout.list!
                                    .cast<Category>()
                                    .map((category) => CategoryButton(
                                          category: category,
                                        ))
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 8,
                          color: Colors.grey[100],
                        ),
                      ],
                    ),
                  if (layout.model == "Post")
                    Column(
                      children: [
                        Marquee(
                          direction: Axis.horizontal,
                          textDirection: TextDirection.ltr,
                          animationDuration: Duration(
                              milliseconds:
                                  layout.list!.cast<Post>().length * 6000),
                          backDuration: Duration(
                              milliseconds:
                                  layout.list!.cast<Post>().length * 6000),
                          pauseDuration: Duration(milliseconds: 1000),
                          directionMarguee: DirectionMarguee.TwoDirection,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: layout.list!
                                  .cast<Post>()
                                  .map((post) => PostItemWidget(
                                        width: 300,
                                        post: post,
                                      ))
                                  .toList(),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 8,
                          color: Colors.grey[100],
                        ),
                      ],
                    ),
                ],
              ));
      }
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: listLayout(),
    );
  }
}

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key? key,
    required this.title,
    this.titleEnd,
    this.pressTitleEnd,
    this.colorTextTitle,
    this.colorTextMore,
  }) : super(key: key);

  final String title;
  final String? titleEnd;
  final Color? colorTextTitle;
  final Color? colorTextMore;

  final GestureTapCallback? pressTitleEnd;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w600,
            color: colorTextTitle ?? Colors.black,
          ),
        ),
        pressTitleEnd == null
            ? Container()
            : GestureDetector(
                onTap: pressTitleEnd,
                child: Text(
                  "${titleEnd == null ? "" : titleEnd}",
                  style: TextStyle(
                      color: colorTextMore ?? Colors.grey[500],
                      fontWeight: FontWeight.w600,
                      fontSize: 13),
                ),
              ),
      ],
    );
  }
}

class CategoryButton extends StatelessWidget {
  const CategoryButton({Key? key, this.category}) : super(key: key);

  final Category? category;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ActionTap.onTap(
          typeAction: mapTypeAction[TYPE_ACTION.CATEGORY_PRODUCT],
          value: category!.id.toString(),
        );
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: HomeButtonWidget(HomeButton(
            title: category!.name!,
            value: category!.id.toString(),
            typeAction: mapTypeAction[TYPE_ACTION.CATEGORY_PRODUCT],
            imageUrl: category!.imageUrl)),
      ),
    );
  }
}
