import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';

import '../../../config_controller.dart';

class HomeStyle4Controller extends GetxController {
  var isTouch = false.obs;
  DataAppCustomerController dataAppCustomerController = Get.find();
  CustomerConfigController configController = Get.find();
  var isRefresh = false.obs;
  Future<void> refresh() async {
    await configController.getAppTheme(refresh: true);
    await dataAppCustomerController.getHomeData();
    await dataAppCustomerController.getBadge();
    await dataAppCustomerController.getInfoCustomer();
    isRefresh.value = !isRefresh.value;
  }
}
