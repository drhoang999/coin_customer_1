import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';

import '../../../config_controller.dart';

class HomeStyle5Controller extends GetxController {
  var isTouch = false.obs;
  DataAppCustomerController dataAppCustomerController = Get.find();
  CustomerConfigController configController = Get.find();
  var isRefresh = false.obs;
  Future<void> refresh() async {
    await configController.getAppTheme(refresh: true);
    await dataAppCustomerController.getHomeData();
    await dataAppCustomerController.getInfoCustomer();
    await dataAppCustomerController.getBadge();
    isRefresh.value = !isRefresh.value;
  }
}
