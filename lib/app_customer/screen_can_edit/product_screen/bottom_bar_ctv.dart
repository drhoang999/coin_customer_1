import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/components/modal/modal_bottom_option_buy_product.dart';
import 'package:sahashop_customer/app_customer/model/order.dart';
import 'package:sahashop_customer/app_customer/model/product.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/product_screen/product_controller.dart';
import 'package:sahashop_customer/app_customer/screen_default/chat_customer_screen/chat_user_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/ctv_customer/list_product_rose/item/content_ctv.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';
import 'package:sahashop_customer/app_customer/utils/color_utils.dart';

class BottomBarCtv extends StatelessWidget {
  ProductController productController;
  BottomBarCtv({required this.productController});
DataAppCustomerController dataAppCustomerController = Get.find();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 65,
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 50,
                    child: InkWell(
                      onTap: () {
                        ModalBottomOptionBuyProduct
                            .showModelOption(
                            product: productController
                                .productShow.value,
                            onSubmit: (int quantity,
                                Product product,
                                List<DistributesSelected>
                                distributesSelected) async {
                              await productController
                                  .addManyItemOrUpdate(
                                  quantity:
                                  quantity,
                                  buyNow: false,
                                  productId:
                                  product.id!,
                                  distributesSelected:
                                  distributesSelected);
                              productController
                                  .animatedAddCard();
                              dataAppCustomerController
                                  .getBadge();
                            });
                      },
                      child: Container(
                        height: 20,
                        width: 20,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: SvgPicture.asset(
                            "packages/sahashop_customer/assets/icons/add_to_cart.svg",
                            color: SahaColorUtils()
                                .colorPrimaryTextWithWhiteBackground(),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: InkWell(
                    onTap: () async {
                        Get.to(() => ContentCtv(
                              productShow: productController.productShow.value,
                            ));
                    },
                    child: Container(
                        margin: EdgeInsets.all(5),
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).primaryColor.withOpacity(0.25),
                        ),
                        child: Center(
                            child: Text(
                          "ĐĂNG BÁN",
                          style: TextStyle(
                            color: SahaColorUtils()
                                .colorPrimaryTextWithWhiteBackground(),
                          ),
                        ))),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: InkWell(
                    onTap: () {
                      ModalBottomOptionBuyProduct.showModelOption(
                          product: productController.productShow.value,
                          onSubmit: (int quantity, Product product,
                              List<DistributesSelected> distributesSelected) {
                            productController.addManyItemOrUpdate(
                                quantity: quantity,
                                buyNow: true,
                                productId: product.id!,
                                lineItemId: null,
                                distributesSelected: distributesSelected);
                          });
                    },
                    child: Container(
                        margin: EdgeInsets.all(5),
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).primaryColor,
                        ),
                        child: Center(
                            child: Text(
                          "MUA NGAY",
                          style: TextStyle(
                              color: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6!
                                  .color),
                        ))),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}
