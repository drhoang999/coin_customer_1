import 'button_home.dart';
import 'product.dart';
import 'banner.dart';
import 'category.dart';
import 'post.dart';

enum HomeLayoutEnum {
  BUTTONS,
  CATEGORY,
  PRODUCTS_DISCOUNT,
  PRODUCTS_TOP_SALES,
  PRODUCTS_NEW,
  POSTS_NEW,
}

Map homeLayoutMap = {
  HomeLayoutEnum.BUTTONS: "HOME_BUTTON",
  HomeLayoutEnum.CATEGORY: "CATEGORY",
  HomeLayoutEnum.PRODUCTS_DISCOUNT: "PRODUCTS_DISCOUNT",
  HomeLayoutEnum.PRODUCTS_TOP_SALES: "PRODUCTS_TOP_SALES",
  HomeLayoutEnum.PRODUCTS_NEW: "PRODUCTS_NEW",
  HomeLayoutEnum.POSTS_NEW: "POSTS_NEW",
};

class HomeData {
  HomeData({
    this.listLayout,
    this.banner,
    this.popups,
  });

  List<LayoutHome>? listLayout;
  BannerList? banner;
  List<Popup>? popups;

  factory HomeData.fromJson(Map<String, dynamic> json) => HomeData(
      banner: BannerList.fromJson(json["banner"]),
      popups: json["popups"] == null
          ? null
          : List<Popup>.from(json["popups"].map((x) => Popup.fromJson(x))),
      listLayout: List<LayoutHome>.from(
          json["layouts"].map((x) => LayoutHome.fromJson(x))));
}

class LayoutHome {
  String? title;
  String? model;
  String? typeLayout;
  String? typeActionMore;
  bool? hide;
  List<dynamic>? list;

  LayoutHome(
      {this.title,
      this.model,
      this.typeLayout,
      this.typeActionMore,
      this.hide,
      this.list});

  factory LayoutHome.fromJson(Map<String, dynamic> json) {
    var list = [];

    if (json["list"] != null && json["list"] is List) {
      if (json["model"] == "HomeButton")
        list = List<HomeButton>.from(
            json["list"].map((x) => HomeButton.fromJson(x)));

      if (json["model"] == "Product")
        list = List<Product>.from(json["list"].map((x) => Product.fromJson(x)));

      if (json["model"] == "Category")
        list =
            List<Category>.from(json["list"].map((x) => Category.fromJson(x)));

      if (json["model"] == "Post")
        list = List<Post>.from(json["list"].map((x) => Post.fromJson(x)));
    }

    return LayoutHome(
      title: json["title"],
      typeLayout: json["type_layout"],
      model: json["model"],
      typeActionMore: json["type_action_more"],
      hide: json["hide"],
      list: list,
    );
  }

  Map<String, dynamic> toJson() => {
        "title": title,
        "type_layout": typeLayout,
        "type_action_more": typeActionMore,
        "hide": hide
      };
}

class BannerList {
  BannerList({
    this.name,
    this.type,
    this.list,
  });

  String? name;
  String? type;
  List<BannerItem>? list;

  factory BannerList.fromJson(Map<String, dynamic> json) => BannerList(
        name: json["name"],
        type: json["type"],
        list: List<BannerItem>.from(
            json["list"].map((x) => BannerItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "type": type,
        "list": List<dynamic>.from(list!.map((x) => x.toJson())),
      };
}

class Popup {
  Popup({
    this.id,
    this.storeId,
    this.name,
    this.linkImage,
    this.showOnce,
    this.typeAction,
    this.valueAction,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  int? storeId;
  String? name;
  String? linkImage;
  bool? showOnce;
  String? typeAction;
  String? valueAction;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Popup.fromJson(Map<String, dynamic> json) => Popup(
        id: json["id"] == null ? null : json["id"],
        storeId: json["store_id"] == null ? null : json["store_id"],
        name: json["name"] == null ? null : json["name"],
        linkImage: json["link_image"] == null ? null : json["link_image"],
        showOnce: json["show_once"] == null ? null : json["show_once"],
        typeAction: json["type_action"] == null ? null : json["type_action"],
        valueAction: json["value_action"] == null ? null : json["value_action"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );
}
