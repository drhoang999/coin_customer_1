import 'dart:convert';

InfoCustomer infoCustomerFromJson(String str) =>
    InfoCustomer.fromJson(json.decode(str));

String infoCustomerToJson(InfoCustomer data) => json.encode(data.toJson());

class InfoCustomer {
  InfoCustomer({
    this.id,
    this.storeId,
    this.username,
    this.phoneNumber,
    this.phoneVerifiedAt,
    this.email,
    this.emailVerifiedAt,
    this.name,
    this.dateOfBirth,
    this.avatarImage,
    this.point,
    this.sex,
    this.createdAt,
    this.updatedAt,
    this.isCollaborator = false,
    this.isAgency = false,
    this.defaultAddress,
  });

  int? id;
  int? storeId;
  dynamic username;
  String? phoneNumber;
  dynamic phoneVerifiedAt;
  dynamic email;
  dynamic emailVerifiedAt;
  dynamic name;
  DateTime? dateOfBirth;
  dynamic avatarImage;
  bool? isCollaborator;
  bool? isAgency;
  double? point;
  dynamic sex;
  DateTime? createdAt;
  DateTime? updatedAt;
  DefaultAddress? defaultAddress;

  factory InfoCustomer.fromJson(Map<String, dynamic> json) => InfoCustomer(
        id: json["id"],
        storeId: json["store_id"],
        username: json["username"],
        phoneNumber: json["phone_number"],
        phoneVerifiedAt: json["phone_verified_at"],
        email: json["email"],
        emailVerifiedAt: json["email_verified_at"],
        name: json["name"],
        dateOfBirth: json["date_of_birth"] == null
            ? null
            : DateTime.parse(json["date_of_birth"]),
        avatarImage: json["avatar_image"],
        point: json["points"] == null ? null : json["points"].toDouble(),
        sex: json["sex"],
        isCollaborator:
            json["is_collaborator"] == null ? false : json["is_collaborator"],
        isAgency: json["is_agency"] == null ? false : json["is_agency"],
        defaultAddress: json["default_address"] == null
            ? null
            : DefaultAddress.fromJson(json["default_address"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "date_of_birth": dateOfBirth!.toIso8601String(),
        "avatar_image": avatarImage,
        "sex": sex,
      };
}

class DefaultAddress {
  DefaultAddress({
    this.id,
    this.storeId,
    this.customerId,
    this.name,
    this.addressDetail,
    this.country,
    this.province,
    this.district,
    this.wards,
    this.village,
    this.postcode,
    this.email,
    this.phone,
    this.isDefault,
    this.createdAt,
    this.updatedAt,
    this.provinceName,
    this.districtName,
    this.wardsName,
  });

  int? id;
  int? storeId;
  int? customerId;
  String? name;
  String? addressDetail;
  int? country;
  int? province;
  int? district;
  int? wards;
  dynamic village;
  dynamic postcode;
  String? email;
  String? phone;
  bool? isDefault;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? provinceName;
  String? districtName;
  String? wardsName;

  factory DefaultAddress.fromJson(Map<String, dynamic> json) => DefaultAddress(
        id: json["id"] == null ? null : json["id"],
        storeId: json["store_id"] == null ? null : json["store_id"],
        customerId: json["customer_id"] == null ? null : json["customer_id"],
        name: json["name"] == null ? null : json["name"],
        addressDetail:
            json["address_detail"] == null ? null : json["address_detail"],
        country: json["country"] == null ? null : json["country"],
        province: json["province"] == null ? null : json["province"],
        district: json["district"] == null ? null : json["district"],
        wards: json["wards"] == null ? null : json["wards"],
        village: json["village"],
        postcode: json["postcode"],
        email: json["email"] == null ? null : json["email"],
        phone: json["phone"] == null ? null : json["phone"],
        isDefault: json["is_default"] == null ? null : json["is_default"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        provinceName:
            json["province_name"] == null ? null : json["province_name"],
        districtName:
            json["district_name"] == null ? null : json["district_name"],
        wardsName: json["wards_name"] == null ? null : json["wards_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "store_id": storeId == null ? null : storeId,
        "customer_id": customerId == null ? null : customerId,
        "name": name == null ? null : name,
        "address_detail": addressDetail == null ? null : addressDetail,
        "country": country == null ? null : country,
        "province": province == null ? null : province,
        "district": district == null ? null : district,
        "wards": wards == null ? null : wards,
        "village": village,
        "postcode": postcode,
        "email": email == null ? null : email,
        "phone": phone == null ? null : phone,
        "is_default": isDefault == null ? null : isDefault,
        "province_name": provinceName == null ? null : provinceName,
        "district_name": districtName == null ? null : districtName,
        "wards_name": wardsName == null ? null : wardsName,
      };
}
