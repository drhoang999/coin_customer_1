class Badge {
  Badge({
    this.ordersWaitingForProgressing = 0,
    this.ordersPacking = 0,
    this.ordersShipping = 0,
    this.ordersNoReviews = 0,
    this.chatsUnread = 0,
    this.postsUnread = 0,
    this.cartQuantity = 0,
    this.favoriteProducts = 0,
    this.statusCollaborator = 0,
    this.statusAgency = 0,
    this.customerPoint = 0,
    this.voucherTotal = 0,
    this.domain,
    this.linkAppple,
    this.linkCh,
    this.productsDiscount = 0,
    this.totalBoughtAmount = 0,
    this.notificationUnread,
    this.allowUsePointOrder = false,
  });

  int? ordersWaitingForProgressing;
  int? ordersPacking;
  int? ordersShipping;
  int? postsUnread;
  int? ordersNoReviews;
  int? chatsUnread;
  int? cartQuantity;
  int? favoriteProducts;
  int? statusCollaborator;
  int? statusAgency;
  double? customerPoint;
  String? domain;
  String? linkAppple;
  String? linkCh;
  int? voucherTotal;
  int? productsDiscount;
  int? notificationUnread;
  double? totalBoughtAmount;
  bool? allowUsePointOrder;

  factory Badge.fromJson(Map<String, dynamic> json) => Badge(
        ordersWaitingForProgressing:
            json["orders_waitting_for_progressing"] == null
                ? null
                : json["orders_waitting_for_progressing"],
        ordersPacking:
            json["orders_packing"] == null ? null : json["orders_packing"],
        ordersShipping:
            json["orders_shipping"] == null ? null : json["orders_shipping"],
        ordersNoReviews: json["orders_no_reviews"] == null
            ? null
            : json["orders_no_reviews"],
        chatsUnread: json["chats_unread"] == null ? null : json["chats_unread"],
        postsUnread: json["posts_unread"] == null ? null : json["posts_unread"],
        domain: json["domain"] == null ? null : json["domain"],
        linkCh: json["link_google_play"] == null ? null : json["link_google_play"],
        linkAppple: json["link_apple_store"] == null ? null : json["link_apple_store"],
        cartQuantity:
            json["cart_quantity"] == null ? null : json["cart_quantity"],
        favoriteProducts: json["favorite_products"] == null
            ? null
            : json["favorite_products"],
      statusCollaborator:  json["status_collaborator"] == null
          ? null
          : json["status_collaborator"],
    statusAgency:  json["status_agency"] == null
        ? null
        : json["status_agency"],
        customerPoint:
            json["customer_point"] == null ? null : json["customer_point"].toDouble(),
        voucherTotal:
            json["voucher_total"] == null ? null : json["voucher_total"],
        productsDiscount: json["products_discount"] == null
            ? null
            : json["products_discount"],
        totalBoughtAmount: json["total_bought_amount"] == null
            ? null
            : json["total_bought_amount"].toDouble(),
      allowUsePointOrder: json["allow_use_point_order"] == null
          ? null
          : json["allow_use_point_order"],
      notificationUnread: json["notification_unread"] == null
          ? null
          : json["notification_unread"],
      );
}
