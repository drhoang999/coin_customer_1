import 'dart:convert';

import 'agency_price.dart';
import 'category.dart';

Product productFromJson(String str) => Product.fromJson(jsonDecode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  Product({
    this.id,
    this.name,
    this.storeId,
    this.description,
    this.indexImageAvatar,
    this.price,
    this.minPrice = 0,
    this.maxPrice,
    this.barcode,
    this.status,
    this.quantityInStock,
    this.quantityInStockWithDistribute,
    this.view,
    this.sold,
    this.likes,
    this.isNew,
    this.isTopSale,
    this.isFavorite,
    this.createdAt,
    this.updatedAt,
    this.distributes,
    this.attributes,
    this.images,
    this.categories,
    this.categoryChildren,
    this.productDiscount,
    this.hasInDiscount,
    this.hasInCombo,
    this.agencyPrice,
    this.percentCollaborator = 0,
    this.contentForCollaborator,
  });

  int? id;
  String? name;
  int? storeId;
  String? description;
  int? indexImageAvatar;
  double? price;
  double? minPrice;
  double? maxPrice;
  String? barcode;
  int? quantityInStock;
  int? quantityInStockWithDistribute;
  int? sold;
  int? view;
  int? likes;
  int? status;
  bool? isNew;
  bool? isTopSale;
  bool? isFavorite;
  DateTime? createdAt;
  DateTime? updatedAt;
  List<Distributes>? distributes;
  List<Attributes>? attributes;
  List<ImageProduct>? images;
  List<Category>? categories;
  List<Category>? categoryChildren;
  ProductDiscount? productDiscount;
  bool? hasInDiscount;
  bool? hasInCombo;
  AgencyPrice? agencyPrice;
  int? percentCollaborator;
  String? contentForCollaborator;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      id: json["id"],
      name: json["name"],
      storeId: json["store_id"],
      description: json["description"],
      indexImageAvatar: 0,
      productDiscount: json["product_discount"] == null
          ? null
          : ProductDiscount.fromJson(json["product_discount"]),
      hasInDiscount: json["has_in_discount"],
      hasInCombo: json["has_in_combo"],
      price: double.tryParse(json["price"].toString()) ?? 0,
      minPrice: double.tryParse(json["min_price"].toString()) ?? 0,
      maxPrice: double.tryParse(json["max_price"].toString()) ?? 0,
      barcode: json["barcode"],
      status: json["status"],
      quantityInStock: json["quantity_in_stock"],
      quantityInStockWithDistribute:
          json["quantity_in_stock_with_distribute"] == null
              ? null
              : json["quantity_in_stock_with_distribute"],
      sold: json["sold"],
      view: json["view"],
      likes: json['likes'],
      isNew: json['is_new'] ?? false,
      agencyPrice: json["agency_price"] == null
          ? null
          : AgencyPrice.fromJson(json["agency_price"]),
      isFavorite: json['is_favorite'] ?? false,
      isTopSale: json['is_top_sale'] ?? false,
      createdAt: json["created_at"] == null
          ? null
          : DateTime.parse(json["created_at"]),
      updatedAt: json["updated_at"] == null
          ? null
          : DateTime.parse(json["updated_at"]),
      distributes: json['distributes'] != null && json['distributes'] is List
          ? List<Distributes>.from(
              json["distributes"].map((x) => Distributes.fromJson(x)))
          : <Distributes>[],
      attributes: json['attributes'] != null && json['attributes'] is List
          ? List<Attributes>.from(
              json["attributes"].map((x) => Attributes.fromJson(x)))
          : <Attributes>[],
      images: json["images"] == null
          ? null
          : List<ImageProduct>.from(
              json["images"].map((x) => ImageProduct.fromJson(x))),
      categories: json["categories"] == null
          ? null
          : List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
      categoryChildren: json["category_children"] == null ? null : List<Category>.from(json["category_children"].map((x) => Category.fromJson(x))),
      percentCollaborator: json["percent_collaborator"] == null ? 0 : json["percent_collaborator"],
      contentForCollaborator: json["content_for_collaborator"] == null ? null : json["content_for_collaborator"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "store_id": storeId,
        "description": description,
        "index_image_avatar": 0,
        "price": price,
        "barcode": name,
        "status": status,
        "likes": likes,
        "quantity_in_stock": quantityInStock,
        "sold": sold,
        "view": view,
        "product_discount":
            productDiscount == null ? null : productDiscount!.toJson(),
        "has_in_discount": hasInDiscount,
        "has_in_combo": hasInCombo,
        "distributes": distributes == null
            ? null
            : List<dynamic>.from(distributes!.map((x) => x.toJson())),
        "attributes": attributes == null
            ? null
            : List<dynamic>.from(attributes!.map((x) => x.toJson())),
        "images":
            images == null ? null : images!.map((e) => e.imageUrl).toList(),
        "categories":
            categories == null ? null : categories!.map((e) => e.id).toList(),
        "percent_collaborator": percentCollaborator,
        "content_for_collaborator": contentForCollaborator,
      };
}

class ImageProduct {
  ImageProduct({
    this.id,
    this.imageUrl,
  });

  int? id;

  String? imageUrl;

  factory ImageProduct.fromJson(Map<String, dynamic> json) => ImageProduct(
        id: json["id"],
        imageUrl: json["image_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image_url": imageUrl,
      };
}

class ProductDiscount {
  ProductDiscount({
    this.value,
    this.discountPrice,
  });

  double? value;

  double? discountPrice;

  factory ProductDiscount.fromJson(Map<String, dynamic> json) =>
      ProductDiscount(
        value: json["value"] == null ? null : json["value"].toDouble(),
        discountPrice: json["discount_price"] == null
            ? null
            : double.tryParse(json["discount_price"].toString())!.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "value": value,
        "discount_price": discountPrice,
      };
}

class Distributes {
  int? id;
  String? name;
  String? subElementDistributeName;
  String? createdAt;
  String? updatedAt;
  List<ElementDistributes>? elementDistributes;

  Distributes({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.elementDistributes,
    this.subElementDistributeName,
  });

  Distributes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    subElementDistributeName = json['sub_element_distribute_name'] == null
        ? null
        : json['sub_element_distribute_name'];
    if (json['element_distributes'] != null) {
      elementDistributes = [];
      json['element_distributes'].forEach((v) {
        elementDistributes!.add(new ElementDistributes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.elementDistributes != null) {
      data['element_distributes'] =
          this.elementDistributes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ElementDistributes {
  String? name;
  String? imageUrl;
  double? price;
  int? quantityInStock;
  List<SubElementDistribute>? subElementDistribute;
  String? createdAt;
  String? updatedAt;

  ElementDistributes(
      {this.name, this.imageUrl, this.createdAt, this.updatedAt});

  ElementDistributes.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    imageUrl = json['image_url'];
    price = json['price'] == null ? null : json["price"].toDouble();
    quantityInStock =
        json['quantity_in_stock'] == null ? null : json['quantity_in_stock'];
    subElementDistribute = json["sub_element_distributes"] == null
        ? null
        : List<SubElementDistribute>.from(json["sub_element_distributes"]
            .map((x) => SubElementDistribute.fromJson(x)));
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image_url'] = this.imageUrl;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Attributes {
  int? id;
  int? storeId;
  int? productId;
  String? name;
  String? value;
  String? createdAt;
  String? updatedAt;

  Attributes(
      {this.id,
      this.storeId,
      this.productId,
      this.name,
      this.value,
      this.createdAt,
      this.updatedAt});

  Attributes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    productId = json['product_id'];
    name = json['name'];
    value = json['value'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['product_id'] = this.productId;
    data['name'] = this.name;
    data['value'] = this.value;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class SubElementDistribute {
  SubElementDistribute({
    this.name,
    this.price,
    this.quantityInStock,
  });

  String? name;
  double? price;
  int? quantityInStock;

  factory SubElementDistribute.fromJson(Map<String, dynamic> json) =>
      SubElementDistribute(
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"].toDouble(),
        quantityInStock: json["quantity_in_stock"] == null
            ? null
            : json["quantity_in_stock"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "quantity_in_stock": quantityInStock == null ? null : quantityInStock,
      };
}
