
import 'package:multi_image_picker2/multi_image_picker2.dart';

class ImageData {
  Asset? file;
  String? linkImage;
  bool? errorUpload;
  bool? uploading;

  ImageData({this.file, this.linkImage, this.errorUpload, this.uploading});
}