import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sahashop_customer/app_customer/socket/socket.dart';
import 'config_controller.dart';
import 'remote/customer_service_manager.dart';
import 'screen_default/data_app_controller.dart';
import 'screen_default/data_app_screen.dart';
import 'package:get/get.dart';
import 'screen_default/navigation_scrren/navigation_screen.dart';
import 'utils/device_info.dart';
import 'utils/store_info.dart';
import 'utils/thread_data.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class SahaShopCustomer extends StatelessWidget {
  final String storeCode;
  final String storeName;
  final bool isPreview;
  final String? slogan;
  final Color? sloganColor;

  const SahaShopCustomer(
      {Key? key,
      required this.storeCode,
      required this.storeName,
      this.isPreview = false,
      this.slogan,
      this.sloganColor});

  @override
  Widget build(BuildContext context) {
    CustomerServiceManager.initialize();
    DeviceInfo().initDeviceId();
    ////////////////////////////////////////////////
    StoreInfo().setCustomerStoreCode(storeCode);
    StoreInfo().name = storeName;
    ////////////////////////////////////////////////
    SocketCustomer().connect();
    CustomerConfigController configController =
        Get.isRegistered() ? Get.find() : Get.put(CustomerConfigController());
    DataAppCustomerController dataAppCustomerController =
        Get.isRegistered() ? Get.find() : Get.put(DataAppCustomerController());

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    if (isPreview == true) {
      FlowData().setIsOnline(true);

      return LoadAppScreen(
        logo: configController.configApp.logoUrl,
        slogan: slogan,
        sloganColor: sloganColor,
      );
    } else {
      FlowData().setIsOnline(true);
      return GetMaterialApp(
        title: 'SahaShop',
        themeMode: ThemeMode.light,
        debugShowCheckedModeBanner: false,
        home: LoadAppScreen(
          logo: configController.configApp.logoUrl,
          slogan: slogan,
          sloganColor: sloganColor,
        ),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        getPages: [
          GetPage(name: "/customer_home", page: () => NavigationScreen())
        ],
        supportedLocales: [
          Locale('vi', 'VN'),
        ],
      );
    }
  }
}
