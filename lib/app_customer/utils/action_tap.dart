import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/category_product_screen/category_product_screen.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/category_product_screen/input_model_products.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/product_screen/product_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/category_post_screen/category_post_screen_1.dart';
import 'package:sahashop_customer/app_customer/screen_default/category_post_screen/read_post_screen/input_model_post.dart';
import 'package:sahashop_customer/app_customer/screen_default/category_post_screen/read_post_screen/read_post_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/chat_customer_screen/chat_user_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/choose_combo/choose_combo_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/choose_voucher/choose_voucher_customer_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/member/member_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/qr_screen/qr_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/web_view/web_view_screen.dart';
import 'package:url_launcher/url_launcher.dart';

enum TYPE_ACTION {
  QR,
  SCORE,
  CALL,
  MESSAGE_TO_SHOP,
  VOUCHER,
  PRODUCTS_TOP_SALES,
  PRODUCTS_DISCOUNT,
  COMBO,
  PRODUCTS_NEW,
  //////
  LINK,
  PRODUCT,
  CATEGORY_PRODUCT,
  CATEGORY_POST,
  POST,
}

final Map mapTypeAction = {
  TYPE_ACTION.QR: "QR",
  TYPE_ACTION.SCORE: "SCORE",
  TYPE_ACTION.CALL: "CALL",
  TYPE_ACTION.MESSAGE_TO_SHOP: "MESSAGE_TO_SHOP",
  TYPE_ACTION.VOUCHER: "VOUCHER",
  TYPE_ACTION.PRODUCTS_TOP_SALES: "PRODUCTS_TOP_SALES",
  TYPE_ACTION.PRODUCTS_DISCOUNT: "PRODUCTS_DISCOUNT",
  TYPE_ACTION.PRODUCTS_NEW: "PRODUCTS_NEW",
  TYPE_ACTION.COMBO: "COMBO",
  ////
  TYPE_ACTION.LINK: "LINK",
  TYPE_ACTION.PRODUCT: "PRODUCT",
  TYPE_ACTION.CATEGORY_PRODUCT: "CATEGORY_PRODUCT",
  TYPE_ACTION.CATEGORY_POST: "CATEGORY_POST",
  TYPE_ACTION.POST: "POST",
};

var checkTypeDefault = [
  TYPE_ACTION.QR,
  TYPE_ACTION.SCORE,
  TYPE_ACTION.CALL,
  TYPE_ACTION.MESSAGE_TO_SHOP,
  TYPE_ACTION.VOUCHER,
  TYPE_ACTION.PRODUCTS_TOP_SALES,
  TYPE_ACTION.PRODUCTS_DISCOUNT,
  TYPE_ACTION.COMBO,
  TYPE_ACTION.PRODUCTS_NEW,
];

class ActionTap {
  static void onTap({String? typeAction, String? value, Function? thenAction}) {
    if (typeAction == mapTypeAction[TYPE_ACTION.QR]) {
      Get.to(() => QRScreen());
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.CALL]) {
      launch("tel:$value");
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.SCORE]) {
      Get.to(() => MemberScreen());
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.MESSAGE_TO_SHOP]) {
      Get.to(() => ChatCustomerScreen());
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.PRODUCTS_TOP_SALES]) {
      Get.to(() => CategoryProductScreen(
            filterProducts: FILTER_PRODUCTS.TOP_SALE,
            categoryId: -1,
          ));
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.PRODUCTS_DISCOUNT]) {
      Get.to(CategoryProductScreen(
        filterProducts: FILTER_PRODUCTS.DISCOUNT,
        categoryId: -1,
      ));
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.PRODUCTS_NEW]) {
      Get.to(CategoryProductScreen(
        filterProducts: FILTER_PRODUCTS.NEW,
        categoryId: -1,
      ));
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.VOUCHER]) {
      Get.to(() => ChooseVoucherCustomerScreen());
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.COMBO]) {
      Get.to(() => ChooseComboScreen());
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.LINK]) {
      Navigator.push(
        Get.context!,
        MaterialPageRoute(
            builder: (context) => WebViewScreen(
                  link: value,
                )),
      ).then((value) {
        if (thenAction != null) {
          thenAction();
        }
      });
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.PRODUCT]) {
      Navigator.push(
        Get.context!,
        MaterialPageRoute(
            builder: (context) => ProductScreen(
                  productId: int.tryParse(value!) ?? -1,
                )),
      ).then((value) {
        if (thenAction != null) {
          thenAction();
        }
      });
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.CATEGORY_PRODUCT]) {
      Navigator.push(
        Get.context!,
        MaterialPageRoute(
            builder: (context) => CategoryProductScreen(
                  categoryId: int.tryParse(value!) ?? -1,
                )),
      ).then((value) {
        if (thenAction != null) {
          thenAction();
        }
      });
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.POST]) {
      Navigator.push(
        Get.context!,
        MaterialPageRoute(
            builder: (context) => ReadPostScreen(
                  inputModelPost: InputModelPost(
                    postId: int.tryParse(value!) ?? -1,
                  ),
                )),
      ).then((value) {
        if (thenAction != null) {
          thenAction();
        }
      });
    }

    if (typeAction == mapTypeAction[TYPE_ACTION.CATEGORY_POST]) {
      Navigator.push(
        Get.context!,
        MaterialPageRoute(
            builder: (context) => CategoryPostScreen(
                  categoryId: int.tryParse(value!) ?? -1,
                )),
      ).then((value) {
        if (thenAction != null) {
          thenAction();
        }
      });
    }
  }
}
