import 'package:sahashop_customer/app_customer/remote/customer_service_manager.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/badge/badge_response.dart';
import 'package:sahashop_customer/app_customer/utils/store_info.dart';

import '../handle_error.dart';


class BadgeRepository {
  Future<BadgeResponse?> getBadge() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getBadge(StoreInfo().getCustomerStoreCode()!);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }
}
