import 'package:sahashop_customer/app_customer/remote/customer_service_manager.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/agency/general_info_payment_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/agency/info_payment_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/agency/info_request.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/agency/payment_agency_history_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/agency/report_rose_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/orders/order_history_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/success/success_response.dart';
import 'package:sahashop_customer/app_customer/utils/store_info.dart';
import '../handle_error.dart';

class AgencyCustomerRepository {
  Future<SuccessResponse?> registerAgency(bool isCollab) async {
    try {
      var res = await CustomerServiceManager().service!.registerAgency(
          StoreInfo().getCustomerStoreCode(), {"is_agency": isCollab});
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<InfoPaymentAgencyResponse?> updateInfoAgency(
      InfoPaymentAgencyRequest infoPaymentRequest) async {
    try {
      var res = await CustomerServiceManager().service!.updateInfoAgency(
          StoreInfo().getCustomerStoreCode(), infoPaymentRequest.toJson());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<InfoPaymentAgencyResponse?> getInfoAgency() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getInfoAgency(StoreInfo().getCustomerStoreCode());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<OrderHistoryResponse?> getOrderAgencyHistory(
      int numberPage,
      String search,
      String fieldBy,
      String filterByValue,
      String sortBy,
      String descending,
      String dateFrom,
      String dateTo,
      ) async {
    try {
      var res = await CustomerServiceManager().service!.getOrderAgencyHistory(
          StoreInfo().getCustomerStoreCode(),
          numberPage,
          search,
          fieldBy,
          filterByValue,
          sortBy,
          descending,
          dateFrom,
          dateTo);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<PaymentAgencyHistoryResponse?> getPaymentAgencyHistory() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getPaymentAgencyHistory(StoreInfo().getCustomerStoreCode());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<ReportRoseAgencyResponse?> getReportAgencyRose(int? page) async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getReportAgencyRose(StoreInfo().getCustomerStoreCode(), page);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<SuccessResponse?> receiveMoneyAgency(int year, int month) async {
    try {
      var res = await CustomerServiceManager().service!.receiveMoneyAgency(
          StoreInfo().getCustomerStoreCode(), {"year": year, "month": month});
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<GeneralInfoPaymentAgencyResponse?> getGeneralInfoPaymentAgency() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getGeneralInfoPaymentAgency(StoreInfo().getCustomerStoreCode());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<SuccessResponse?> requestPaymentAgency() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .requestPaymentAgency(StoreInfo().getCustomerStoreCode());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }
}
