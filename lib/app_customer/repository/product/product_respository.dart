import 'package:sahashop_customer/app_customer/data/example/product.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/favorite/all_product_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/product/all_product_response.dart';
import 'package:sahashop_customer/app_customer/remote/response-request/product/product_watched_response.dart';
import 'package:sahashop_customer/app_customer/utils/store_info.dart';
import '../../remote/customer_service_manager.dart';
import '../../remote/response-request/product/detail_product_response.dart';
import '../../repository/handle_error.dart';
import '../../utils/thread_data.dart';
import '../../model/product.dart';

class ProductCustomerRepository {
  Future<List<Product>?> searchProduct(
      {String search = "",
      int page = 1,
      String idCategory = "",
      String idCategoryChild = "",
      bool descending = false,
      String details = "",
      String sortBy = ""}) async {
    if (FlowData().isOnline()) {
      try {
        var res = await CustomerServiceManager().service!.searchProduct(
            StoreInfo().getCustomerStoreCode(),
            page,
            search,
            idCategory,
            idCategoryChild,
            descending,
            details,
            sortBy);
        return res.data!.data;
      } catch (err) {
        handleErrorCustomer(err);
      }
    }else {
      return EXAMPLE_LIST_PRODUCT;
    }
  }

  Future<DetailProductResponse?> getDetailProduct(int? idProduct) async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getDetailProduct(StoreInfo().getCustomerStoreCode(), idProduct);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<AllProductFavorites?> getAllPurchasedProducts({int? page}) async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getPurchasedProducts(StoreInfo().getCustomerStoreCode()!, page!);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<AllProductResponse?> getSimilarProduct(int idProduct) async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getSimilarProduct(StoreInfo().getCustomerStoreCode()!, idProduct);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<ProductWatchedResponse?> getWatchedProduct() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getWatchedProduct(StoreInfo().getCustomerStoreCode()!);
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }
}
