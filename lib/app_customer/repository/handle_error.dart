import 'package:dio/dio.dart';

String handleErrorCustomer(err) {
  if (err is DioError) {
    print("ERROR Dio: ${err.error.toString()}");
    throw err.error.toString();
  } else {
    print(err.toString() + "Lỗi ngoài dio");
    throw throw "Có lỗi xảy ra";
  }
}