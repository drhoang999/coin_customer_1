import 'package:sahashop_customer/app_customer/utils/store_info.dart';

import '../../remote/customer_service_manager.dart';
import '../../remote/response-request/marketing_chanel/combo_customer_response.dart';
import '../../remote/response-request/marketing_chanel/voucher_customer_response.dart';
import '../../components//toast/saha_alert.dart';
import '../handle_error.dart';

class MarketingRepository {
  Future<CustomerComboResponse?> getComboCustomer() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getComboCustomer(StoreInfo().getCustomerStoreCode());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }

  Future<VoucherCustomerResponse?> getVoucherCustomer() async {
    try {
      var res = await CustomerServiceManager()
          .service!
          .getVoucherCustomer(StoreInfo().getCustomerStoreCode());
      return res;
    } catch (err) {
      handleErrorCustomer(err);
    }
  }
}
