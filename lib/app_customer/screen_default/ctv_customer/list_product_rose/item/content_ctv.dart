import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sahashop_customer/app_customer/components/empty/saha_empty_image.dart';
import 'package:sahashop_customer/app_customer/components/loading/loading_ios.dart';
import 'package:sahashop_customer/app_customer/components/toast/saha_alert.dart';
import 'package:sahashop_customer/app_customer/model/product.dart';
import 'package:sahashop_customer/app_customer/utils/color_utils.dart';
import 'package:sahashop_customer/app_customer/utils/store_info.dart';
import 'package:share/share.dart';
import 'package:http/http.dart' as http;

import '../../../data_app_controller.dart';

class ContentCtv extends StatelessWidget {
  Product productShow;
  ContentCtv({required this.productShow});

  DataAppCustomerController dataAppCustomerController = Get.find();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.black)),
            Text(
              "Nội dung đăng bán",
              style: TextStyle(color: Colors.black),
            ),
          ],
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 8,
              color: Colors.grey[200],
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: double.infinity,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      "*Có thể thay đổi nội dung theo cá nhân đăng bán",
                      style: TextStyle(color: Colors.blueAccent),
                    ),
                  ),
                  Container(
                    width: Get.width,
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: 10.0,
                      runSpacing: 10.0,
                      children: productShow.images!
                          .map(
                            (e) => Container(
                              width: (Get.width - 50) / 4,
                              height: (Get.width - 50) / 4,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(4),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(4),
                                  child: CachedNetworkImage(
                                    imageUrl: e.imageUrl ?? "",
                                    fit: BoxFit.cover,
                                    errorWidget: (context, url, error) =>
                                        SahaEmptyImage(),
                                  ),
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("Link mua hàng:"),
                  Text(
                    "${dataAppCustomerController.badge.value.domain ?? "${StoreInfo().getCustomerStoreCode()}.mydoapp.vn"}/san-pham/${productShow.id}?cowc_id=${dataAppCustomerController.infoCustomer.value.id}",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("${productShow.contentForCollaborator ?? ""}")
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
          height: 65,
          color: Colors.white,
          child: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: InkWell(
                        onTap: () {
                          Clipboard.setData(ClipboardData(
                              text:
                                  "Link mua hàng: ${dataAppCustomerController.badge.value.domain ?? "${StoreInfo().getCustomerStoreCode()}.mydoapp.vn"}/san-pham/${productShow.id}?cowc_id=${dataAppCustomerController.infoCustomer.value.id}\n\n${productShow.contentForCollaborator ?? ""}"));
                          SahaAlert.showSuccess(
                            message: "Đã sao chép",
                          );
                        },
                        child: Container(
                            margin: EdgeInsets.all(5),
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Theme.of(context).primaryColor,
                            ),
                            child: Center(
                                child: Text(
                              "SAO CHÉP",
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .primaryTextTheme
                                      .headline6!
                                      .color),
                            ))),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: InkWell(
                        onTap: () async {
                          urlToFile(productShow.images!
                              .map((e) => e.imageUrl)
                              .toList());
                        },
                        child: Container(
                            margin: EdgeInsets.all(5),
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(0.25),
                            ),
                            child: Center(
                                child: Text(
                              "ĐĂNG BÁN",
                              style: TextStyle(
                                color: SahaColorUtils()
                                    .colorPrimaryTextWithWhiteBackground(),
                              ),
                            ))),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              )
            ],
          )),
    );
  }

  Future<void> urlToFile(List<String?>? listUrl) async {
    LoadingiOS().onLoading();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    imagePaths = [];
    if (listUrl != null) {
      for (int i = 0; i <= listUrl.length - 1; i++) {
        if (listUrl[i] != null) {
          print(listUrl[i]);
          File file = new File('$tempPath' + ('$i' + '.jpg'));
          http.Response response = await http.get(Uri.parse(listUrl[i]!));
          await file.writeAsBytes(response.bodyBytes);
          imagePaths.add(file.path);
          print(imagePaths);
          print(i);
          if (i == listUrl.length - 1) {
            Get.back();
            _onShare();
          }
        }
      }
    }
  }

  List<String> imagePaths = [];
  Future<void> _onShare() async {
    final box = Get.context!.findRenderObject() as RenderBox?;
    if (imagePaths.isNotEmpty) {
      await Share.shareFiles(imagePaths,
          text:
              "Link mua hàng: https://${StoreInfo().getCustomerStoreCode()}.mydoapp.vn/san-pham/${productShow.id}?cowc_id=${dataAppCustomerController.infoCustomer.value.id}\n\n${productShow.contentForCollaborator ?? ""}",
          subject: "",
          sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size);
    } else {
      await Share.share(productShow.contentForCollaborator ?? "",
          subject: "",
          sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size);
    }
  }
}
