import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../config_controller.dart';
import '../../screen_can_edit/home/home_screen.dart';
import '../../screen_default/cart_screen/cart_screen.dart';
import '../../screen_default/category_post_screen/category_post_screen_1.dart';
import '../../screen_default/order_history/order_history_screen.dart';
import '../../screen_default/profile_screen/profile_screen.dart';

class NavigationController extends GetxController {
  var selectedIndexBottomBar = 2.obs;
  late CustomerConfigController configController;
  List<Widget> navigationHome = [];

  NavigationController() {
    configController = Get.find();

    navigationHome = [
      CartScreen(
        key: GlobalKey(),
      ),
      Container(
        key: PageStorageKey<String>('str1'),
        child: CategoryPostScreen(),
      ),
      Container(key: GlobalKey(), child: HomeScreen()),
      OrderHistoryScreen(
        key: PageStorageKey<String>('str3'),
      ),
      ProfileScreen(
        key: PageStorageKey<String>('str4'),
      ),
    ];
  }
}
