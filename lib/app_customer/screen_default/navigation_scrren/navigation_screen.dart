import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:sahashop_customer/app_customer/components/popup/popup.dart';
import 'package:sahashop_customer/app_customer/utils/color_utils.dart';
import '../../screen_default/data_app_controller.dart';
import 'navigation_controller.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  NavigationController navigationController = NavigationController();
  DataAppCustomerController dataAppCustomerController = Get.find();
  final PageStorageBucket _bucket = PageStorageBucket();

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Future.delayed(const Duration(milliseconds: 2000), () {
        if (dataAppCustomerController.popups.isNotEmpty && dataAppCustomerController.isShowed == false) {
          PopupShow.showPopup();
          dataAppCustomerController.isShowed = true;
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> pages = navigationController.navigationHome;

    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Obx(
        () => Scaffold(
          body: PageStorage(
            child: pages[navigationController.selectedIndexBottomBar.value],
            bucket: _bucket,
          ),
          bottomNavigationBar: BottomNavigationBar(
            unselectedLabelStyle: TextStyle(color: Colors.grey),
            unselectedItemColor: Colors.grey,
            showUnselectedLabels: true,
            type: BottomNavigationBarType.fixed,
            selectedItemColor:
                SahaColorUtils().colorPrimaryTextWithWhiteBackground(),
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Badge(
                  padding: EdgeInsets.all(3),
                  toAnimate: true,
                  animationType: BadgeAnimationType.slide,
                  badgeContent: Text(
                    '${dataAppCustomerController.badge.value.cartQuantity}',
                    style: TextStyle(fontSize: 11, color: Colors.white),
                  ),
                  showBadge:
                      dataAppCustomerController.badge.value.cartQuantity == 0
                          ? false
                          : true,
                  child: Icon(Ionicons.cart),
                ),
                label: 'Giỏ hàng',
              ),
              BottomNavigationBarItem(
                icon: Badge(
                  padding: EdgeInsets.all(3),
                  toAnimate: true,
                  animationType: BadgeAnimationType.slide,
                  badgeContent: Text(
                    '${dataAppCustomerController.badge.value.postsUnread}',
                    style: TextStyle(fontSize: 11, color: Colors.white),
                  ),
                  showBadge:
                      dataAppCustomerController.badge.value.postsUnread == 0
                          ? false
                          : true,
                  child: Icon(Ionicons.newspaper),
                ),
                label: 'Tin tức',
              ),
              BottomNavigationBarItem(
                icon: Icon(Ionicons.storefront),
                label: 'Trang chủ',
              ),
              BottomNavigationBarItem(
                icon: Badge(
                  padding: EdgeInsets.all(3),
                  toAnimate: true,
                  animationType: BadgeAnimationType.slide,
                  badgeContent: Text(
                    '${dataAppCustomerController.badge.value.ordersWaitingForProgressing}',
                    style: TextStyle(fontSize: 11, color: Colors.white),
                  ),
                  showBadge: dataAppCustomerController
                              .badge.value.ordersWaitingForProgressing ==
                          0
                      ? false
                      : true,
                  child: Icon(Ionicons.receipt),
                ),
                label: 'Đơn hàng',
              ),
              BottomNavigationBarItem(
                icon: Icon(Ionicons.person),
                label: 'Tài khoản',
              ),
            ],
            currentIndex: navigationController.selectedIndexBottomBar.value,
            onTap: (currentIndex) {
              navigationController.selectedIndexBottomBar.value = currentIndex;
            },
          ),
        ),
      ),
    );
  }
}
