import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/components/empty/saha_empty_image.dart';
import 'package:sahashop_customer/app_customer/components/loading/loading_widget.dart';
import 'package:sahashop_customer/app_customer/utils/date_utils.dart';
import 'input_model_post.dart';
import 'read_post_controller.dart';

// ignore: must_be_immutable
class ReadPostScreen extends StatelessWidget {
  InputModelPost? inputModelPost;
  late PostController postController;

  ReadPostScreen({this.inputModelPost}) {
    postController = PostController(inputModelPost);
  }
  Html? html;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text(
            "Tin tức"),
      ),
      body: Obx(
        () => Stack(
          children: [
            postController.isLoadingPost.value
                ? Container()
                :   SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CachedNetworkImage(
                    height: Get.height * 0.24,
                    width: Get.width,
                    fit: BoxFit.cover,
                    imageUrl: postController.postShow.value.imageUrl ?? "",
                    errorWidget: (context, url, error) => SahaEmptyImage(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Tin tức ngày: ${SahaDateUtils().getDDMMYY(DateTime.parse(postController.postShow.value.updatedAt!))}",
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey[600],
                          ),
                          maxLines: 2,
                        ),
                        Text("${postController.postShow.value.title ?? ""}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),),
                        Text("${postController.postShow.value.summary ?? ""}", style: TextStyle(fontSize: 17),)
                      ],
                    ),
                  ),
                  Html(
                    data: postController.postShow.value.content ?? " ",
                  ),
                ],
              ),
            ),

                    postController.isLoadingPost.value
                ? Center(
                    child: SahaLoadingWidget(),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
