import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/components/empty_widget/saha_empty_customer_widget.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';
import 'package:sahashop_customer/app_customer/utils/color_utils.dart';

import 'code_introduce_controller.dart';

class CodeRetroduceScreen extends StatelessWidget {
  CodeRetroduceController codeRetroduceController = CodeRetroduceController();
  DataAppCustomerController dataAppCustomerController = Get.find();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Mã giới thiệu"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey[400]!),
                              color: Colors.grey[200],
                              shape: BoxShape.circle),
                          padding: EdgeInsets.all(5),
                          child: SvgPicture.asset(
                            'packages/sahashop_customer/assets/icons/group_empty.svg',
                            height: 30,
                            width: 30,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          children: [
                            Text(
                              "Mã giới thiệu",
                              style: TextStyle(
                                  color: SahaColorUtils()
                                      .colorPrimaryTextWithWhiteBackground(),
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${dataAppCustomerController.infoCustomer.value.phoneNumber ?? "Chưa xác thực"}",
                              style: TextStyle(
                                  fontSize: 15, color: Colors.blueAccent),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Spacer(),
                IconButton(
                    onPressed: () {
                      codeRetroduceController.onShare(
                          dataAppCustomerController
                              .infoCustomer.value.phoneNumber,
                          dataAppCustomerController.packageInfo.value.appName);
                    },
                    icon: Icon(
                      Icons.share,
                      color: SahaColorUtils()
                          .colorPrimaryTextWithWhiteBackground(),
                    ))
              ],
            ),
          ),
          Container(
            height: 8,
            color: Colors.grey[200],
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Danh sách đã giới thiệu:",
              style: TextStyle(
                  color: SahaColorUtils().colorPrimaryTextWithWhiteBackground(),
                  fontWeight: FontWeight.w600),
            ),
          ),
          Divider(
            height: 1,
          ),
          Obx(
            () => Expanded(
              child: SingleChildScrollView(
                child: codeRetroduceController.listPhoneType.isEmpty
                    ? Center(
                        child: SahaEmptyCustomerWidget(
                          width: Get.width / 3,
                          height: Get.width / 3,
                        ),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: codeRetroduceController.listPhoneType
                            .map((e) => Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                          "Người dùng: ${e.referencesValue ?? "Chưa rõ"}"),
                                    ),
                                    Divider(
                                      height: 1,
                                    ),
                                  ],
                                ))
                            .toList(),
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
