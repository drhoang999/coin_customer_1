import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/components/toast/saha_alert.dart';
import 'package:sahashop_customer/app_customer/model/score_history_item.dart';
import 'package:sahashop_customer/app_customer/repository/repository_customer.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';
import 'package:share/share.dart';

class CodeRetroduceController extends GetxController {
  var listPhoneType = RxList<ScoreHistoryItem>([]);

  DataAppCustomerController dataAppCustomerController = Get.find();

  CodeRetroduceController() {
    getHistoryScore();
  }

  Future<void> onShare(
    String? phone,
    String? appName,
  ) async {
    final box = Get.context!.findRenderObject() as RenderBox?;
    await Share.share(
        "Mời bạn tải ứng dụng ${appName ?? "DoApp"}, nhập mã giới thiệu $phone để nhận ngay các Voucher bạn mới hấp dẫn. Tải ứng dụng tại đây ${Platform.isAndroid ? "${dataAppCustomerController.badge.value.linkCh ?? "https://play.google.com/store/apps/details?id=com.doapp.shopuser"}" : "${dataAppCustomerController.badge.value.linkAppple ?? "https://apps.apple.com/vn/app/doapp-t%E1%BA%A1o-app-b%C3%A1n-h%C3%A0ng/id1582486194"}"}",
        subject: "",
        sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size);
  }

  Future<void> getHistoryScore() async {
    try {
      var data =
          await CustomerRepositoryManager.scoreRepository.getScoreHistory();
      data!.data!.data!.forEach((e) {
        if (e.type == "REFERRAL_CUSTOMER") {
          listPhoneType.add(e);
        }
      });
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
  }
}
