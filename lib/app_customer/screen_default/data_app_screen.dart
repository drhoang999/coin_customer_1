import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/socket/socket.dart';
import '../config_controller.dart';
import 'data_app_controller.dart';

class LoadAppScreen extends StatefulWidget {
  final String? logo;
  final String? slogan;
  final Color? sloganColor;

  const LoadAppScreen({Key? key, this.logo, this.slogan, this.sloganColor})
      : super(key: key);

  @override
  _LoadAppScreenState createState() => _LoadAppScreenState();
}

class _LoadAppScreenState extends State<LoadAppScreen> {
  CustomerConfigController configController = Get.find();
  DataAppCustomerController dataAppCustomerController = Get.find();

  var isInit = false;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadInit(context);
  }


  Future<String> fetchConfig() async {

    var dio = Dio();
   var response = await dio.get('https://karavui.com/api_news/public/api/config_link');

    if (response.statusCode == 200) {
      try {

        var map = jsonDecode(response.data);
        print(map);


        return "";
      } catch (err) {
        return "";
      }

      return "";
    } else {
      return "";
    }
  }

  Future<void> loadInit(BuildContext context) async {
    isInit = true;
    dataAppCustomerController.checkLogin();

    var toWeb = await fetchConfig();
    await Future.wait([
      configController.getAppTheme(),
      dataAppCustomerController.getHomeData(),
    ]);

    Get.offNamed('customer_home')!.then((value) {SocketCustomer().close();});
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.asset(
                  "assets/logo.png",
                  height: 150,
                  width: 150,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              if (widget.slogan != null)
                Text(
                  widget.slogan!,
                  style: TextStyle(
                      color: widget.sloganColor,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                )
            ],
          ),
        ),
      ),
    );
  }
}
