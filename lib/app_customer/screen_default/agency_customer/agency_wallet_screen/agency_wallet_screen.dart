import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sahashop_customer/app_customer/model/step_bonus.dart';
import 'package:sahashop_customer/app_customer/screen_default/Agency_customer/payment_history/payment_history_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/agency_customer/config_payment_agency_screen/config_payment_ctv_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/agency_customer/order_history/order_history_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/agency_customer/report/report_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/agency_customer/report_rose/report_rose_screen.dart';
import 'package:sahashop_customer/app_customer/screen_default/data_app_controller.dart';
import 'package:sahashop_customer/app_customer/screen_default/login/check_login/check_login.dart';
import 'package:sahashop_customer/app_customer/utils/color_utils.dart';
import 'package:sahashop_customer/app_customer/utils/string_utils.dart';
import '../register_agency_screen.dart';
import 'agency_wallet_controller.dart';
import 'widget/card_function.dart';

class AgencyWalletScreen extends StatelessWidget {
  var height = AppBar().preferredSize.height;

  late List<Widget> itemWidget;

  AgencyWalletController agencyWalletController =
      Get.put(AgencyWalletController());
  DataAppCustomerController dataAppCustomerController = Get.find();
  @override
  Widget build(BuildContext context) {
    itemWidget = [
      historyTransaction(),
    ];
    return CheckCustomerLogin(
      child: RegisterAgencyScreen(
        child: Obx(() {
          if (dataAppCustomerController.badge.value.statusAgency == 0 &&
              dataAppCustomerController.infoCustomer.value.isAgency == true) {
            return Scaffold(
              appBar: AppBar(
                title: Text("Đại lý"),
              ),
              body: Center(
                child: Container(
                  width: Get.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: height,
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          "Chức năng Agency của bạn đã bị chặn!\nHãy liên hệ với chủ shop để được giải quyết",
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Scaffold(
              appBar: AppBar(
                title: Text("Đại lý"),
              ),
              body: SingleChildScrollView(
                child: Column(
                  children: itemWidget,
                ),
              ),
            );
          }
        }),
      ),
    );
  }

  Widget boxTab(
      {required int index, required String title, required Function onTap}) {
    return InkWell(
      onTap: () {
        onTap()!;
      },
      child: Container(
        width: Get.width / 3,
        height: 40,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              height: 5,
            ),
            Obx(
              () => Text(
                title,
                style: TextStyle(
                    fontSize: 15,
                    color: agencyWalletController.indexWidget.value == index
                        ? SahaColorUtils().colorPrimaryTextWithWhiteBackground()
                        : Colors.grey[500]),
              ),
            ),
            Obx(
              () => agencyWalletController.indexWidget.value == index
                  ? Container(
                      height: 3,
                      width: 40,
                      color: SahaColorUtils()
                          .colorPrimaryTextWithWhiteBackground(),
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }

  Widget infomationWallet() {
    return Column(
      children: [
        Obx(
          () => CartFunction(
            title: "Doanh thu tháng này",
            subText:
                "${agencyWalletController.generalInfoPaymentAgency.value.numberOrder ?? 0} Giao dịch",
            totalMoney:
                "${SahaStringUtils().convertToMoney(agencyWalletController.generalInfoPaymentAgency.value.totalFinal ?? 0)}",
            svgAssets: "packages/sahashop_customer/assets/icons/payment.svg",
            svgColor: Colors.green,
            onTap: () {
              Get.to(() => OrderHistoryAgencyScreen(
                    initPage: 3,
                  ));
            },
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      color: Color(0xFFF5F6F9),
                      shape: BoxShape.circle,
                    ),
                    child: SvgPicture.asset(
                      "packages/sahashop_customer/assets/icons/gift_fill.svg",
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "THƯỞNG THEO MỨC DOANH THU",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Divider(height: 1),
              Obx(
                () => Column(
                  children: [
                    ...List.generate(
                      agencyWalletController.generalInfoPaymentAgency.value
                              .stepsBonus?.length ??
                          0,
                      (index) => boxGiftBonus(
                          typeRose: agencyWalletController
                                  .generalInfoPaymentAgency.value.typeRose ??
                              0,
                          svgAsset:
                              "packages/sahashop_customer/assets/icons/point.svg",
                          svgAssetCheck:
                              "packages/sahashop_customer/assets/icons/checked.svg",
                          stepsBonus: agencyWalletController
                              .generalInfoPaymentAgency
                              .value
                              .stepsBonus![index]),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget boxGiftBonus({
    required int typeRose,
    required String svgAsset,
    required String svgAssetCheck,
    required StepsBonus stepsBonus,
  }) {
    return Column(
      children: [
        Container(
          padding:
              const EdgeInsets.only(left: 15.0, right: 15.0, top: 8, bottom: 8),
          decoration: BoxDecoration(
              border: Border.all(
                  color: typeRose == 0
                      ? agencyWalletController
                                  .generalInfoPaymentAgency.value.totalFinal! >=
                              stepsBonus.limit!
                          ? Colors.transparent
                          : Colors.amber
                      : agencyWalletController
                                  .generalInfoPaymentAgency.value.balance! >=
                              stepsBonus.limit!
                          ? Colors.transparent
                          : Colors.amber,
                  width: 1.5)),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: SvgPicture.asset(
                  typeRose == 0
                      ? agencyWalletController
                                  .generalInfoPaymentAgency.value.totalFinal! >=
                              stepsBonus.limit!
                          ? svgAssetCheck
                          : svgAsset
                      : agencyWalletController
                                  .generalInfoPaymentAgency.value.balance! >=
                              stepsBonus.limit!
                          ? svgAssetCheck
                          : svgAsset,
                  height: 25,
                  width: 25,
                ),
              ),
              Text(
                "Đạt: ${SahaStringUtils().convertToMoney(stepsBonus.limit)}₫",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              Spacer(),
              Text(
                "Thưởng: ${SahaStringUtils().convertToMoney(stepsBonus.bonus)}₫",
                style:
                    TextStyle(color: Colors.green, fontWeight: FontWeight.w600),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget payment() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        color: Color(0xFFF5F6F9),
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        "packages/sahashop_customer/assets/icons/wallet.svg",
                        color: SahaColorUtils()
                            .colorPrimaryTextWithWhiteBackground(),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "THÔNG TIN THANH TOÁN",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Divider(height: 1),
                Obx(
                  () => agencyWalletController.isFullInfoPayment.value == true
                      ? Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  "CMND/CCCD",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                              ),
                              Row(
                                children: [
                                  Text("Họ và tên:"),
                                  Spacer(),
                                  Text(
                                      "${agencyWalletController.fullName.value}"),
                                ],
                              ),
                              Row(
                                children: [
                                  Text("Số CMND/CCCD:"),
                                  Spacer(),
                                  Text("${agencyWalletController.cmnd.value}"),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  "Tài khoản ngân hàng",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                              ),
                              Text("${agencyWalletController.bank}"),
                              Text("${agencyWalletController.accountNumber}"),
                            ],
                          ),
                        )
                      : Container(),
                ),
                Obx(
                  () => agencyWalletController.isFullInfoPayment.value == false
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                              "Bạn chưa có thông tin thanh toán.\nVui lòng nhập thông tin để được thanh toán tiền từ ví Agency"),
                        )
                      : Container(),
                ),
                InkWell(
                  onTap: () {
                    Get.to(() => ConfigPaymentAgencyScreen())!.then((value) => {
                          agencyWalletController.getInfoAgency(),
                        });
                  },
                  child: Container(
                    width: Get.width,
                    height: 40,
                    margin: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            Theme.of(Get.context!)
                                .primaryColor
                                .withOpacity(0.7),
                            Theme.of(Get.context!)
                                .primaryColor
                                .withOpacity(0.3),
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [0.0, 1.0],
                          tileMode: TileMode.repeated),
                      borderRadius: BorderRadius.circular(40),
                    ),
                    child: Center(
                      child: Obx(() => Text(
                            "${agencyWalletController.isFullInfoPayment.value == false ? "THÊM THÔNG TIN THANH TOÁN" : "SỬA THÔNG TIN THANH TOÁN"}",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
          Container(
            height: 8,
            color: Colors.grey[200],
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        color: Color(0xFFF5F6F9),
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        "packages/sahashop_customer/assets/icons/wallet.svg",
                        color: SahaColorUtils()
                            .colorPrimaryTextWithWhiteBackground(),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "CHÍNH SÁCH THANH TOÁN",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Divider(height: 1),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                            "Tiền từ Ví Agency của thành viên Diamond sẽ được thanh toán định kỳ ${agencyWalletController.generalInfoPaymentAgency.value.payment1OfMonth == true && agencyWalletController.generalInfoPaymentAgency.value.payment16OfMonth == true ? "02/Tháng" : agencyWalletController.generalInfoPaymentAgency.value.payment1OfMonth == true || agencyWalletController.generalInfoPaymentAgency.value.payment16OfMonth == true ? "01/Tháng" : ""}",
                          ),
                        ),
                        if (agencyWalletController.generalInfoPaymentAgency
                                .value.payment16OfMonth ==
                            true)
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: SvgPicture.asset(
                                    "packages/sahashop_customer/assets/icons/point.svg",
                                    height: 25,
                                    width: 25,
                                  ),
                                ),
                                Text(
                                  "Ngày 15 hàng tháng",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        if (agencyWalletController.generalInfoPaymentAgency
                                .value.payment1OfMonth ==
                            true)
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: SvgPicture.asset(
                                    "packages/sahashop_customer/assets/icons/point.svg",
                                    height: 25,
                                    width: 25,
                                  ),
                                ),
                                Text(
                                  "Ngày 30 hàng tháng",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text:
                                        "* Lưu ý:\nBạn cần có số dư ví tối thiểu là ",
                                    style: TextStyle(color: Colors.black)),
                                TextSpan(
                                    text:
                                        "${SahaStringUtils().convertToMoney(agencyWalletController.generalInfoPaymentAgency.value.paymentLimit ?? 0)} VNĐ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.red)),
                                TextSpan(
                                    text: " để được thanh toán định kỳ",
                                    style: TextStyle(color: Colors.black)),
                              ],
                            ),
                          ),
                        ),
                        if (agencyWalletController.generalInfoPaymentAgency
                                .value.hasPaymentRequest ==
                            false)
                          Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  agencyWalletController.requestPaymentAgency();
                                },
                                child: Container(
                                  width: Get.width,
                                  height: 40,
                                  margin: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                          Theme.of(Get.context!)
                                              .primaryColor
                                              .withOpacity(0.7),
                                          Theme.of(Get.context!)
                                              .primaryColor
                                              .withOpacity(0.3),
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        stops: [0.0, 1.0],
                                        tileMode: TileMode.repeated),
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "GỬI YÊU CẦU THANH TOÁN",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                  "* Lưu ý:\nBạn cần gửi yêu cầu thanh toán trước ít nhất 3 ngày so với kỳ hạn thanh toán"),
                            ],
                          ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget historyTransaction() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Obx(
          () => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Cấp đại lý:"),
                Text("${agencyWalletController.nameAgency.value}"),
              ],
            ),
          ),
        ),
        Divider(height: 1,),
        CartFunction(
          title: "Các đơn hàng đã đặt",
          svgAssets: "packages/sahashop_customer/assets/icons/check_list.svg",
          svgColor: Colors.teal,
          onTap: () {
            Get.to(() => OrderHistoryAgencyScreen());
          },
        ),
        // CartFunction(
        //   title: "Lịch sử thay đổi số dư",
        //   svgAssets: "packages/sahashop_customer/assets/icons/money.svg",
        //   svgColor: Colors.amber,
        //   onTap: () {
        //     Get.to(() => PaymentHistoryAgencyScreen());
        //   },
        // ),
        CartFunction(
          title: "Báo cáo tổng quan",
          svgAssets: "packages/sahashop_customer/assets/icons/report.svg",
          svgColor: Colors.pink,
          onTap: () {
            Get.to(() => ReportAgencyScreen());
          },
        ),
      ],
    );
  }
}
