import 'dart:async';

import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:sahashop_customer/app_customer/components/toast/saha_alert.dart';
import 'package:sahashop_customer/app_customer/model/badge.dart';
import 'package:sahashop_customer/app_customer/model/home_data.dart';
import 'package:sahashop_customer/app_customer/model/info_customer.dart';
import 'package:sahashop_customer/app_customer/model/post.dart';
import 'package:sahashop_customer/app_customer/model/roll_call.dart';
import 'package:sahashop_customer/app_customer/repository/repository_customer.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/category_product_screen/input_model_products.dart';
import 'package:sahashop_customer/app_customer/screen_can_edit/product_screen/input_model.dart';
import 'package:sahashop_customer/app_customer/utils/customer_info.dart';
import 'cart_screen/cart_controller.dart';
import 'category_post_screen/input_model_posts.dart';

class DataAppCustomerController extends GetxController {
  InputModelProduct? inputModelProduct;
  InputModelProducts? inputModelProducts;
  InputModelPosts? inputModelPosts;

  Post? postCurrent;
  var badge = Badge().obs;
  HomeData? homeData = HomeData();
  var popups = RxList<Popup>();
  var indexPopup = 0.obs;
  int? scoreToday;
  var isShowed = false;
  var isCheckIn = false;
  var isLogin = false.obs;
  var infoCustomer = InfoCustomer().obs;

  CartController cartController = Get.put(CartController());

  DataAppCustomerController() {
    startTimerLoadData();
    initPackageInfo();
  }

  var packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  ).obs;

  Future<void> initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    packageInfo.value = info;
  }

  void startTimerLoadData() {
    const oneSec = const Duration(seconds: 60 * 1);
    new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (isLogin.value == true) {
          getBadge();
        }
      },
    );
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getHomeData();
  }

  Future<void> logout() async {
    badge.value = Badge();
  }

  Future<void> checkLogin() async {
    if (await CustomerInfo().hasLogged()) {
      getInfoCustomer();
      getBadge();
      cartController.getItemCart();
      isLogin.value = true;
    } else {
      isLogin.value = false;
      // noLogin(context);
    }
  }

  Future<void> checkIn() async {
    try {
      var data = await CustomerRepositoryManager.scoreRepository.checkIn();
      SahaAlert.showSuccess(message: "Điểm danh thành công");
      isCheckIn = false;
      getBadge();
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
  }

  Future<void> getInfoCustomer() async {
    if (await CustomerInfo().hasLogged()) {
      try {
        var res = await CustomerRepositoryManager.infoCustomerRepository
            .getInfoCustomer();
        infoCustomer.value = res!.data!;

        isLogin.value = true;
      } catch (err) {
        // SahaAlert.showError(message: err.toString());
        isLogin.value = false;
      }
    } else {
      isLogin.value = false;
    }
  }

  Future<bool?> getHomeData() async {
    try {
      var data = await CustomerRepositoryManager.homeDataCustomerRepository
          .getHomeData();
      homeData = data;
      popups(data!.popups!);
      return true;
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
  }

  /// Badge

  Future<void> getBadge() async {
    try {
      var data = await CustomerRepositoryManager.badgeRepository.getBadge();
      badge.value = data!.data!;
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
  }
}
