import 'package:flutter/material.dart';
import 'package:sahashop_customer/app_customer/sahashop_customer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:sahashop_customer/app_customer/load_data/load_firebase.dart';

const STORE_CODE = "sy";
const STORE_NAME = "Sỹ Shop";

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("Handling a background message: ${message.messageId}");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  LoadFirebase.initFirebase();

  runApp(SahaShopCustomer(
    storeCode: STORE_CODE,
    storeName: STORE_NAME,
  ));
}
